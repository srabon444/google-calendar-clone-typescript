# Google Calendar Clone

This google calendar clone is created using React, Typescript, Javascript, HTML and CSS. This application can be used to create events, delete events, update events, and view events. Events are stored in the local storage of the browser.


## [Live Application](https://google-calendar-clone-typescript.vercel.app/)

![App Screenshot 1](public/1.png)
![App Screenshot 2](public/2.png)
![App Screenshot 3](public/3.png)
![App Screenshot 4](public/4.png)

## Features

- Create events
- Delete events
- Update events
- View events
- Events are stored in the local storage of the browser
- User can switch between different months

## Tech Stack
- React
- Typescript
- HTML
- Javascript
- CSS

**Hosting:**
- Vercel


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/srabon444/google-calendar-clone-typescript.git
```

Install dependencies

```bash
  npm install
```
