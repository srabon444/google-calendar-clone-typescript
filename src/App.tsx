import Calendar from "./components/Calendar.tsx";
import "./styles.css";
import { EventsProvider } from "./context/Events.tsx";

export default function App() {
  return (
    <EventsProvider addEvent={event => event}>
      <Calendar />
    </EventsProvider>
  );
}
