// Following UnionOmit type is used to remove a key from a union of objects.

export type UnionOmit<T, K extends string | number | symbol> = T extends unknown
  ? Omit<T, K>
  : never;