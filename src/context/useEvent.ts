import {useContext} from "react";
import {Context} from "./Events.tsx";

// EVENT_COLORS is an array of strings that represent the colors of the events.
export const EVENT_COLORS = ["red", "green", "blue"] as const;

// useEvents is a custom hook that returns the events context.
export function useEvents(){
    const value = useContext(Context);
    if (value == null) {
        throw new Error("useEvent must be used within an EventsProvider");
    }

    return value;
}